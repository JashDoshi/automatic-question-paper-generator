public class Question extends Chapter{
	private String question;
	// private int questionId;
	private int difficulty;
	private int chapterId;
	private int marks;
	private int probability;

	Question(int chapterId,String chapterName,int subjectCode,String subjectName,int teacherId,String quest,int difficulty,int marks,int probability){
		super(chapterId,chapterName,subjectCode,subjectName,teacherId);
		this.question=quest;
		// this.questionId=questionId;
		this.difficulty=difficulty;
		this.marks=marks;
		this.probability=probability;
		insertQuery();
	}
	void insertQuery(){
		System.out.println("Question : "+question);
		System.out.println("Difficulty : "+difficulty);
		System.out.println("Marks : "+marks);
	}
}