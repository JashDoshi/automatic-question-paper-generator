/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author vijay
 */
import java.io.FileOutputStream;
import java.util.Date;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.util.ArrayList;
import javax.swing.JOptionPane;


public class FirstPdf {
    static ArrayList<String> questions=new ArrayList<>();
    static ArrayList<Integer> questionNos=new ArrayList<>();
    static ArrayList<Integer> marks=new ArrayList<>();
    static int subCode;
    static int totalMarks;
    static int time;
    static int i=0;
    private static String FILE = "C:/temp/FirstPdf.pdf";
    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);
    private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.NORMAL, BaseColor.RED);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);
    
    public static void addQuestionPdf(int questionNo,String question,int mark){
        questionNos.add(questionNo+i);
        questions.add(question);
        marks.add(mark);
        i++;
        
    }
    FirstPdf(){
        try {
            subCode=MainClass.getSubCode();
            totalMarks=MainClass.getTotalMarks();
            if(totalMarks>=80)
                time=3;
            else if(totalMarks>=40 && totalMarks<80)
                time=2;
            else if(totalMarks>=0 && totalMarks<40)
                time=1;
                
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            JOptionPane.showMessageDialog(null,"document open");
            addMetaData(document);
            addTitlePage(document);
            addContent(document);
            document.close();
            JOptionPane.showMessageDialog(null,"pdf created confirm");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // iText allows to add metadata to the PDF which can be viewed in your Adobe
    // Reader
    // under File -> Properties
    public static void addMetaData(Document document) {
        JOptionPane.showMessageDialog(null,"pdf metadata");
        document.addTitle("My first PDF");
        document.addSubject("Using iText");
        document.addKeywords("Java, PDF, iText");
        document.addAuthor("Lars Vogel");
        document.addCreator("Lars Vogel");
    }

    public static void addTitlePage(Document document)
            throws DocumentException {
        Paragraph preface = new Paragraph();
        // We add one empty line
        addEmptyLine(preface, 1);
        // Lets write a big header
        Paragraph p=new Paragraph();
        
        p.add(new Paragraph(subCode+"", catFont));
        p.setAlignment(1);
        document.add(p);
        
        preface.add(new Paragraph(time+" Hours / "+totalMarks+" Marks", subFont));
        preface.setAlignment(0);
//        addEmptyLine(preface, 1);
        preface.add(new Paragraph("___________________________________________________________________________"));
        addEmptyLine(preface, 1);
        preface.add(new Paragraph("Instruction :       (1) All questions are compulsary"));
        preface.add(new Paragraph("                          (2) Answer each next main Question on next page"));
        preface.add(new Paragraph("                          (3) Figures to right indicate full marks"));
        preface.add(new Paragraph("                          (4) Assume suitable data, if necessary"));
        addEmptyLine(preface, 2);

        document.add(preface);
//        // Start a new page
//        document.newPage();
    }

    public static void addContent(Document document) throws DocumentException {

        Paragraph subPara = new Paragraph("Questions", subFont);
        subPara.setAlignment(0);
        Paragraph paragraph = new Paragraph();
        addEmptyLine(paragraph, 3);
        
        // add a table
        createTable(subPara);        
        document.add(subPara);

    }

    public static void createTable(Paragraph subPara){
        float[] columnWidth={1,5,1};
        PdfPTable table = new PdfPTable(columnWidth);
        PdfPCell c1 = new PdfPCell(new Phrase("Question No"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Phrase("Question"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);

        c1 = new PdfPCell(new Paragraph("Marks"));
        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(c1);
        
        for(int i=0;i<questionNos.size();i++){
            table.addCell(questionNos.get(i)+"");
            table.addCell(questions.get(i));
            table.addCell(marks.get(i)+"");
        }
        table.setWidthPercentage(100);
        subPara.add(table);

    }

    public static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}
