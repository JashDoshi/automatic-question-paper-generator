public class Subject extends Teacher{
	private String subjectName;
	private int subjectCode;
	private int teacherId;

	Subject(int teacherId,String subjectName,int subjectCode){
		super(teacherId);
		this.subjectName=subjectName;
		this.subjectCode=subjectCode;
		
		insertQuery2();
	}
	void insertQuery2(){
		System.out.println("Subject Name : "+subjectName);
		System.out.println("Subject Code : "+subjectCode);
	}
}