package dbmsconnection;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class MysqlConnect {
    Connection conn;
    /*
    Returns a valid Connection Object if it can establish connection with emp
    Else returns null
    */
    public static Connection connectDB(){
        try{
            Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/questionsforaqpg","jash","jash");
//            JOptionPane .showMessageDialog(null,"Connection Established Successfully");
            return conn;
        }catch(SQLException e){
            JOptionPane .showMessageDialog(null,"Connection Failed Why??"+e);
            return null;
        }
    }
}
