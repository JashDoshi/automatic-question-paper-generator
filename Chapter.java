public class Chapter extends Subject{
	private int chapterId;
	private String chapterName;
	private int subjectCode;

	Chapter(int chapterId,String chapterName,int subjectCode,String subjectName,int teacherId){
		super(teacherId,subjectName,subjectCode);
		this.chapterId=chapterId;
		this.chapterName=chapterName;
		// System.out.println("inside chapter");
		insertQuery1();
	}
	void insertQuery1(){
		System.out.println("Chapter Name : "+chapterName);
		System.out.println("Chapter Id : "+chapterId);
	}
}