import dbmsconnection.MysqlConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import javax.swing.JOptionPane;

class MainClass{
	String questionToAdd="";
	Connection conn;
	PreparedStatement ps;
	ResultSet rs;
//	Scanner scanner;
	private int probability=0;
	private HashSet<String> questionHash = new HashSet<>();
	int numbers[]=new int[200];
	static int totalMarks=0;
        int noOfQuestion;
	String chpNo;
	String diff;
	String str1="";
        static int subCode;
        int difficulty=0;
        int sum;
        
	MainClass(int subCode,int totalMarks,String chpNo,String diff){
            conn=MysqlConnect.connectDB();
            this.subCode=subCode;
            this.totalMarks=totalMarks;
            this.chpNo=chpNo;
            this.diff=diff;
            noOfQuestion=(totalMarks/8+totalMarks/4)/2;
            if(diff.equalsIgnoreCase("Easy"))
                difficulty=1;
            else if(diff.equalsIgnoreCase("Moderate"))
                difficulty=2;
            else if(diff.equalsIgnoreCase("Tough"))
                difficulty=3;
//            JOptionPane.showMessageDialog(null,"inside");
            JOptionPane.showMessageDialog(null,"inside const"+subCode+" "+totalMarks+" "+chpNo+" "+difficulty);
                
//		question=new HashSet();
//		scanner=new Scanner(System.in);
//		randomQuestionPaper();	
	}
        public static int getSubCode(){
            return subCode;
        }
        public static int getTotalMarks(){
            return totalMarks;
        }
        String randomQuestionPaper(){
		
		try{
		// 	String str="SELECT * FROM question WHERE id=? AND chap_id=? AND difficulty=?";
		//	ps=conn.prepareStatement(str);
	 	//  rs=ps.executeQuery();
			String sort="SELECT * FROM question WHERE difficulty=? AND sub_code=? ORDER BY probability ASC";
			ps=conn.prepareStatement(sort);
                        ps.setString(1,difficulty+"");
                        ps.setString(2,subCode+"");
			rs=ps.executeQuery();
			rs.next();
			if(rs.first()){
				probability=rs.getInt("probability");
				System.out.println(probability+"\n");
			}

			String chp="SELECT * FROM chapter WHERE sub_code=?";
			ps=conn.prepareStatement(chp);
//			System.out.println("\nEnter Subject Code: ");
//			int subCode=scanner.nextInt();
//			scanner.nextLine();
			ps.setInt(1,subCode);
			rs=ps.executeQuery();
			if(rs.next()){

//				System.out.println("Enter the total marks: ");
////				totalMarks=scanner.nextInt();
////				scanner.nextLine();
//				System.out.println("Enter Chapter Nos: ");
//				chpNo=scanner.nextLine();
//				System.out.println("Enter the Difficulty: ");
//				diff=scanner.nextLine();
				System.out.println("got the subject");
                questionToAdd=mainQuery();
                // return questionToAdd;
			}

			randomGenerate();
                        
			System.out.println(questionHash.size());
			String questions[]=new String[questionHash.size()];
			int i1=0;
			for(String str1: questionHash){
				questions[i1]=str1;
				i1++;
			}
			// questionHash.clear();
			

			i1=0;
			int marks=0;

			String mark="SELECT * FROM question WHERE question=?";
			int n=-1;
			questionToAdd="";
			while(marks<totalMarks){
				if(n==numbers[i1]){
					System.out.println("Sorry No More Question ): ");
					break;
				}
				ps=conn.prepareStatement(mark);
				ps.setString(1,questions[numbers[i1]]);
				n=numbers[i1];
				// System.out.println("\n"+probability+"  "+questions[numbers[i1]]+"\n");
				rs=ps.executeQuery();
				rs.next();
				marks += rs.getInt("marks");
				int prob=rs.getInt("probability");
				prob+=1;
				questionToAdd+=questions[numbers[i1]]+",";
                                int qMark=rs.getInt("marks");
//                                sum=qMark+marks;
                                System.out.println("the sum is : "+marks);
                                if(marks>totalMarks){
                                    while(marks!=totalMarks){
                                        qMark--;
                                        marks--;
                                    }
                                }
				System.out.println(questions[numbers[i1]]+"----------->"+qMark);
                                new QuestionPdf(questions[numbers[i1]],qMark);                            
				String update="UPDATE question SET probability=? WHERE question=?";
				ps=conn.prepareStatement(update);
				ps.setString(1,prob+"");
				ps.setString(2,questions[numbers[i1]]);
				ps.execute();
				i1++;
			}
		}
		catch(SQLException e){ System.out.println("exception:"+e); }
        return questionToAdd;
	}
        String getQuery(){
            String str="";
            if(diff.equalsIgnoreCase("easy")){
                str="SELECT * FROM question WHERE chp_id=? AND probability=? AND difficulty=1 AND sub_code=?";
            }else if(diff.equalsIgnoreCase("moderate")){
                str="SELECT * FROM question WHERE chp_id=? AND probability=? AND difficulty=2 AND sub_code=?";
            }else if(diff.equalsIgnoreCase("tough")){
                str="SELECT * FROM question WHERE chp_id=? AND probability=? AND difficulty=3 AND sub_code=?";
            }
            return str;
        }
	String mainQuery() throws SQLException{
		System.out.println("inside main query");
		String splitChpNo[]=chpNo.split(",");
		String str=getQuery();
                    
		for(int i=0;i<splitChpNo.length;i++){
                    ps=conn.prepareStatement(str);
                    ps.setString(1,splitChpNo[i]);
                    ps.setString(2,probability+"");
                    ps.setString(3,subCode+"");
                    rs=ps.executeQuery();
                    while(rs.next()){
                        if(questionHash.add(rs.getString("question")));
                            str1+=rs.getString("question")+",";
                    }
                    if(i==splitChpNo.length-1 && questionHash.size()==0){
                            System.out.println("inside if");
                            probability+=1;
                            i=0;
                    }
		}
                if(questionHash.size()<noOfQuestion){
                    probability++;
                    String sql=getQuery();
                    for(int i=0;i<splitChpNo.length;i++){
                        ps=conn.prepareStatement(sql);
                        ps.setString(1,splitChpNo[i]);
                        ps.setString(2,probability+"");
                        ps.setString(3,subCode+"");
                        rs=ps.executeQuery();
                        while(rs.next()){
                            if(questionHash.add(rs.getString("question")));
                                str1+=rs.getString("question")+",";
                        }
                    }
                }
		System.out.println(str1);
            JOptionPane.showMessageDialog(null,str1);
            return str;
	} 

	void randomGenerate(){
		Random rd=new Random();
		HashSet<Integer> hs=new HashSet<>();
		int no;
		int i=0;
		while(hs.size()!=questionHash.size()){
			no=rd.nextInt(questionHash.size());
			if(hs.add(no)){
				numbers[i]=no;
				System.out.println("--------->"+numbers[i]);
				i++;
			}
		}
        hs.clear();
	}
	public static void main(String[] args) {
		// MainClass m=new MainClass(17515,20,"1,2,3,4,5,6","Moderate");
		// String string=m.randomQuestionPaper();
		// System.out.println(string);	
	}
}